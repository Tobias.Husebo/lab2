package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {
    private List<FridgeItem> fridge = new ArrayList<FridgeItem>();
    private int fridge_max_size = 20;


    /**
     *  FRIDGE METHODS:    
    **/ 

    //Returns number of items currently in the fridge:
    public int nItemsInFridge() {
        int numOfItems = fridge.size();
        return numOfItems;
    }

    //Returns max allowable number of items in fridge:
    public int totalSize() {
        return fridge_max_size;
    }

    /** 
     * Method for placing food in fridge, only if avaliable space.
     * If food successfully placed in fridge, return true, else, false.
    **/
    public boolean placeIn(FridgeItem item) {
        int numOfItems = fridge.size();
        if (numOfItems == fridge_max_size) {
            return false;
        } 
        fridge.add(item);
        return true;
    }

    /** 
     * Method for removing item from fridge.
     * If item non-existent, throw exception "NoSuchElementException".
    **/
    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)){
            fridge.remove(item);
        } else {
            throw new NoSuchElementException();
        }
        
    }

    //Empty entire fridge.
    public void emptyFridge() {
        fridge.clear();
    }

    /** 
     * Remove all expired items from fridge.
     * Return list of expired items.
    **/
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> ExpItems = new ArrayList<FridgeItem>();
        for(FridgeItem item : fridge) {
            if (item.hasExpired()){
                ExpItems.add(item);
            }
        }
        for(FridgeItem item : ExpItems) {
            fridge.remove(item);
        }
        return ExpItems;
    }

}
